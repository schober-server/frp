#!/bin/sh

cd "$(dirname "$0")" || exit

docker-compose pull
./restart.sh
